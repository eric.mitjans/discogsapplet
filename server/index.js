// server/index.js
const express = require("express");
const path = require("path");
const PORT = process.env.PORT || 8080;
const app = express();
require("dotenv").config("../.env");

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});

var Discogs = require("disconnect").Client;

// Have Node serve the files for our built React app
app.use(express.static(path.resolve(__dirname, "../client/build")));

// Handle GET requests to /api route
app.get("/api", (req, res) => {
  const page = req.query.page;
  const user = req.query.user;
  var col = new Discogs({
    consumerKey: process.env.CONSUMER_KEY,
    consumerSecret: process.env.CONSUMER_SECRET,
  })
    .user()
    .collection();

  col.getReleases(
    user,
    0,
    { page: page, per_page: 50, sort: "added", sort_order: "desc" },
    function (err, data) {
      res.json({ message: data });
    }
  );
});

// All other GET requests not handled before will return our React app
app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "../client/build", "index.html"));
});
