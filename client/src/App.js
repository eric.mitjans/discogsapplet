// client/src/App.js

import React, { useState, useEffect } from "react";
import logo from "./logo.png";
import logoMobile from "./logoMobile.png";
import Tape from "./Tape.js";
import "./App.scss";
import Form from "./Form.js";
import html2canvas from "html2canvas";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import "bootstrap/dist/css/bootstrap.min.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleQuestion } from "@fortawesome/pro-regular-svg-icons";
import { faCircleXmark } from "@fortawesome/pro-regular-svg-icons";
import { faHeart } from "@fortawesome/free-solid-svg-icons";

function App() {
  const [rawData, setRawData] = useState(null);
  const [collection, setCollection] = useState([]);
  const [page, setPage] = useState(1);
  const [finished, setFinished] = useState(false);
  const [canvas, setCanvas] = useState(null);
  const [loader, setLoader] = useState(false);
  const [error, setError] = useState(false);
  const [empty, setEmpty] = useState(false);
  const [nothingInYear, setNothingInYear] = useState(false);
  const [cols, setCols] = useState(null);
  const [infoBox, setInfoBox] = useState(false);
  const [searchIsOver, setSearchIsOver] = useState(false);
  const [isMobile, setIsMobile] = useState(window.innerWidth < 992);

  const currentYear = new Date().getFullYear();

  const [formData, setFormData] = useState({
    name: "",
    year: currentYear,
  });

  let currentYearRecords = [];

  useEffect(() => {
    window.addEventListener(
      "resize",
      () => {
        const ismobile = window.innerWidth < 992;
        if (ismobile !== isMobile) setIsMobile(ismobile);
      },
      false
    );
  }, [isMobile]);

  // Modal window states
  const [show, setShow] = useState(false);
  const handleClose = () => {
    setShow(false);
    setCanvas(null);
  };
  const handleShow = () => setShow(true);

  // Calls findYTDRecords every time rawData is updated
  useEffect(() => {
    if (rawData && rawData.length > 0) {
      findYTDRecords();
    }
  }, [rawData]);

  // Resets to defaults when finished is true
  useEffect(() => {
    if (finished == true) {
      setLoader(false);
      setPage(1);
    }
  }, [finished]);

  // When canvas is set, opens modal and displays canvas in modal
  useEffect(() => {
    if (show == false && canvas != null) {
      handleShow();
      setTimeout(canvasElement, 400);
    }
  }, [canvas]);

  // Handle changes in formData and turns year into int.
  useEffect(() => {
    if (typeof formData.year === "string") {
      const intYear = parseInt(formData.year);
      setFormData({
        ...formData,
        year: intYear,
      });
    }
  }, [formData]);

  // First call is made by clicking on button
  // Subsequent calls are made until the year of the results > the year of the form
  useEffect(() => {
    if (page > 1) {
      firstBatch();
    }
  }, [page]);

  // Append canvas to modal div#jpeg
  const canvasElement = () => {
    var element = document.createElement("div");
    console.log(canvas);
    element.appendChild(canvas);

    document.getElementById("jpeg").appendChild(element);
  };

  const firstBatch = () => {
    if (page === 1) {
      console.log(formData.name);
      gtag("event", "username", { username: formData.name });
    }
    setLoader(true);
    setFinished(false);
    setError(false);
    setEmpty(false);
    setNothingInYear(false);
    setCols(null);
    setCollection([]);
    setSearchIsOver(false);
    fetch("/api?page=" + page + "&user=" + formData.name)
      .then((res) => res.json())
      .then((data) => {
        if (data.message.releases && data.message.releases.length < 50) {
          console.log("releases array is less than 50");
          setSearchIsOver(true);
        }
        console.log("does it go here?");
        // sequencial calls until we pass the selected year
        if (rawData && page > 1 && data.message.releases.length > 0) {
          const appendData = rawData.concat(data.message.releases);
          setRawData(appendData);
          // first call, user is there and he has releases
        } else if (data.message.releases && data.message.releases.length > 0) {
          const appendData = data.message.releases;
          setRawData(appendData);
          // user exists but there are no releases (Empty case)
        } else if (data.message.releases && data.message.releases.length == 0) {
          setEmpty(true);
          setLoader(false);
          // user doesnt exist, and/or there is no release array in the response (Error case)
        } else if (!data.message.releases || !data.message) {
          setLoader(false);
          setError(true);
        }
      });
  };

  const png = () => {
    setLoader(true);
    html2canvas(document.getElementById("collectionWrapper"), {
      allowTaint: true,
    }).then(function (canvas) {
      setCanvas(canvas);
      setLoader(false);
    });
  };

  const findYTDRecords = () => {
    let continueSearch = true;

    rawData.map((item, index) => {
      // get year added in number form
      let yearAdded = item.date_added.substring(0, 4);
      yearAdded = parseInt(yearAdded);

      if (yearAdded === formData.year) {
        setNothingInYear(false);
        item.index = index;
        item.year = item.date_added.substring(0, 4);
        currentYearRecords.push(item);
      }

      if (
        yearAdded < formData.year ||
        (searchIsOver && yearAdded > formData.year)
      ) {
        continueSearch = false;
        if (currentYearRecords.length == 0) {
          setNothingInYear(true);
        }
      }
    });

    console.log("searchIsOver: " + searchIsOver);
    console.log("continueSearch: " + continueSearch);
    if (continueSearch && searchIsOver === false) {
      console.log("page + 1 and continue");
      let newPage = page + 1;
      setPage(newPage);
    } else {
      console.log("query is over");
      setCollection(currentYearRecords);
      setFinished(true);
    }
  };

  function shuffleArray(originalArray) {
    let array = [...originalArray];
    var currentIndex = array.length,
      temporaryValue = null,
      randomIndex = null;

    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // Swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  const shuffle = async () => {
    const shuffledCollection = await shuffleArray(collection);
    setCollection(shuffledCollection);
  };

  return (
    <div className="app">
      <div
        className={`infoBoxContainer ${infoBox ? "open" : ""} ${
          isMobile ? "isMobile" : ""
        }`}
      >
        <span>
          This app allows you to create a snapshot of your newly added records
          in your Discogs Collection, year by year.
          <br />
          Share your decadent collection growth, make it about the vanity and
          ruin that record collecting can be!
          <br />
          <br /> Created with{" "}
          <FontAwesomeIcon className="heart" icon={faHeart} /> by{" "}
          <a href="https://www.discogs.com/user/Further" target="_blank">
            Eric Mitjans
          </a>
          , using React, NodeJS and the Discogs API.
          <br /> Awesome cassette svg animation by{" "}
          <a href="https://codepen.io/mikomagni/" target="_blank">
            Miko
          </a>
          .
          <br />
          Color gradient animation by{" "}
          <a href="https://codepen.io/P1N2O" target="_blank">
            Manuel Pinto
          </a>
          .
        </span>
        <FontAwesomeIcon
          icon={faCircleXmark}
          className={`infoBox close ${isMobile ? "isMobile top" : ""}`}
          onClick={() => setInfoBox(false)}
        />
      </div>
      {loader == true && (
        <div className="loaderWrapper">
          <Tape />
        </div>
      )}
      <header
        className={`container-fluid mainHeader ${infoBox ? "open" : ""} ${
          isMobile ? "isMobile" : ""
        }`}
      >
        <div className="row">
          <div className="col">
            <img src={isMobile ? logoMobile : logo} className="logo my-4" />
            <div
              className={`infoBox ${isMobile ? "isMobile" : ""}`}
              onClick={() => setInfoBox(!infoBox)}
            >
              <FontAwesomeIcon icon={faCircleQuestion} />
            </div>
            <Form
              setFormData={setFormData}
              firstBatch={firstBatch}
              formData={formData}
              collection={collection}
              empty={empty}
              error={error}
              shuffle={shuffle}
              png={png}
              setCols={setCols}
              isMobile={isMobile}
              nothingInYear={nothingInYear}
            />
            {/*<button className="fetchData" onClick={firstBatch}>
              Fetch records
            </button>*/}
          </div>
        </div>
      </header>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>
            Download your {formData.year} records screenshot! (right-click, Save
            Image As...)
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div id="jpeg"></div>
        </Modal.Body>
      </Modal>
      {collection && collection.length > 0 && (
        <div
          className={`collectionWrapper mt-4 mt-lg-5 ${cols}`}
          id="collectionWrapper"
        >
          {collection.map((item) => (
            <>
              <a
                href={
                  "https://www.discogs.com/release/" + item.basic_information.id
                }
                target="_blank"
                className="recordWrapper"
                key={item.index}
              >
                <div
                  className="record"
                  style={{
                    backgroundImage:
                      "url(" + item.basic_information.cover_image + ")",
                  }}
                ></div>
                <div className="recordOverlay">
                  <h2>{item.basic_information.artists[0].name}</h2>
                  <h3>{item.basic_information.title}</h3>
                </div>
              </a>
            </>
          ))}
        </div>
      )}
    </div>
  );
}

export default App;
