// client/src/Form.js

import React, { useState, useEffect } from "react";
import "./Form.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDownload } from "@fortawesome/pro-regular-svg-icons";
import { faRocket } from "@fortawesome/pro-regular-svg-icons";
import { faShuffle } from "@fortawesome/pro-regular-svg-icons";

function Form(props) {
	const handleChange = (event) => {
		props.setFormData({
			...props.formData,
			[event.target.name]: event.target.value,
		});
	};

	const handleSubmit = (event) => {
		event.preventDefault();
	};

	const handleColChange = (e) => props.setCols(e.target.value);

	const columns = [
		{ value: "", label: "Columns" },
		{ value: "two", label: "2 columns" },
		{ value: "three", label: "3 columns" },
		{ value: "four", label: "4 columns" },
		{ value: "five", label: "5 columns" },
		{ value: "six", label: "6 columns" },
		{ value: "eight", label: "8 columns" },
		{ value: "ten", label: "10 columns" },
		{ value: null, label: "Default (responsive)" },
	];

	const now = new Date().getUTCFullYear();
	const years = Array(now - (now - 20))
		.fill("")
		.map((v, idx) => now - idx);

	return (
		<form onSubmit={handleSubmit} className="form mt-2 mt-sm-4">
			<div className="row">
				<div className="col-12 col-sm-4 mb-2 mb-sm-3">
					<input
						type="text"
						name="name"
						placeholder="Enter your Discogs username"
						value={props.formData.name}
						onChange={handleChange}
					/>
				</div>
				<div className="col-6 col-sm-4 mb-2 mb-sm-3">
					<select
						value={props.formData.year}
						name="year"
						onChange={handleChange}
					>
						{years.map((year, index) => (
							<option key={index} value={year}>
								{year}
							</option>
						))}
						;
					</select>
				</div>
				<div className="col-6 col-sm-4 mb-2 mb-sm-3">
					<button onClick={props.firstBatch}>
						<FontAwesomeIcon icon={faRocket} className="me-3" />
						Let's go!
					</button>
				</div>
			</div>
			{props.collection && props.collection.length > 0 && (
				<div className="row">
					<div className="col-6 col-sm-4">
						<select
							name="columns"
							onChange={(e) => handleColChange(e)}
						>
							{columns.map((col, index) => (
								<option key={index} value={col.value}>
									{col.label}
								</option>
							))}
							;
						</select>
					</div>
					<div className="col-3 col-sm-4">
						<button onClick={props.shuffle}>
							<FontAwesomeIcon
								icon={faShuffle}
								className={`${props.isMobile ? "" : "me-3"}`}
							/>
							{!props.isMobile && <span>Shuffle!</span>}
						</button>
					</div>
					<div className="col-3 col-sm-4">
						<button onClick={props.png}>
							<FontAwesomeIcon
								icon={faDownload}
								className={`${props.isMobile ? "" : "me-3"}`}
							/>
							{!props.isMobile && (
								<span>Download screenshot</span>
							)}
						</button>
					</div>
				</div>
			)}
			{props.error == true && (
				<div class="row">
					<div className="col">
						<div className="error mt-3">
							The username you inputed doesn't exist, or their
							collection is not public!
						</div>
					</div>
				</div>
			)}
			{props.empty == true && (
				<div class="row">
					<div className="col">
						<div className="error mt-3">
							The username exists but they have no records in
							their collection!
						</div>
					</div>
				</div>
			)}
			{props.nothingInYear == true && (
				<div class="row">
					<div className="col">
						<div className="error mt-3">
							The username exists but no records were added in the
							chosen year!
						</div>
					</div>
				</div>
			)}
		</form>
	);
}

export default Form;
